# Switchboard

Companion app for the AFRC Connecting the Network guide

## About the App

Switchboard is a cross-platform (Android and iOS) mobile application designed
to be a companion of the Connecting the Network resiliency resource developed
over at A1Z.

### Why the name Switchboard

The name Switchboard refers to the nostalgic time where telephone companies
employed switchboard operators to connect callers to the world. Using this
analogy, the Switchboard app connects users to a plethora of resilience resources
around the Wing, AFRC, Air Force, Dod, and National agencies and organizations.

## Code Contributions (Getting Started)

This project is open to anyone who would like to make contributions to it.

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

Here are a few other resources to help you get up-to-speed with Flutter:
- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)
